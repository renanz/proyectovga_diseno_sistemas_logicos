`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:48:14 12/17/2017 
// Design Name: 
// Module Name:    ProyectoVGA 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ProyectoVGA(
	input clk100mhz,
	output reg vsync,
	output reg hsync,
	output reg [2:0]red,
	output reg [2:0]green,
	output reg [1:0]blue
);

	wire vga_clk;
	
	//Covertir frecuencia del CLK de 100MHz a uno de 25MHz
	DCM_SP #(.CLKFX_DIVIDE(8), .CLKFX_MULTIPLY(2), .CLKIN_PERIOD(10)) 
		vga_clock_dcm (.CLKIN(clk100mhz), .CLKFX(vga_clk), .CLKFB(1'd0), .PSEN(1'd0), .RST(1'd0));
	
	//Contadores
	reg [9:0] hcount; 
	reg [9:0] vcount;
	reg [9:0] cont;
	
	//Pseudocodigo proporcionado.
	always @(posedge vga_clk) begin	
		//AreaVisible + Porche frontal + pulso + porche trasero
		if(hcount == 799) begin 
			hcount = 10'd0;
			if(vcount == 524)
				vcount = 10'd0;
			else 
				vcount = vcount + 10'd1;
		end
		else 
			hcount = hcount + 10'd1;
		
		//area visible + porche frontal  y area visible + porche frontal + pulso
		if(vcount>=490 & vcount<492) begin
			vsync = 0;
			cont = cont + 10'd1; //Contador para limitar velocidad del cubo
		end
		else 
			vsync = 1;
		
		//area visible + porche frontal  y area visible + porche frontal + pulso
		if(hcount>=656 & hcount<752) begin
			hsync = 0;
		end
		else 
			hsync = 1;
	end
	
	
	
	//Logica de la figura
	
	//Cuadrado
	parameter radioSqr = 20;
	
	//Posiciones iniciales
	parameter x_init = 160;
	parameter y_init = 100;
	reg [9:0] xPos = x_init;
	reg [9:0] yPos = y_init;
	//Bordes del cuadrado
	reg [9:0] leftWall;
	reg [9:0] rightWall;
	reg [9:0] topWall;
	reg [9:0] bottomWall;
	
	//En que direccion se esta moviendo
	reg up = 10'd1;
	reg down = 10'd0;
	reg left = 10'd1;
	reg right = 10'd0;
	
	//Asignar los limites del contorno del cuadrado
	always @(posedge vga_clk) begin
		leftWall = xPos - radioSqr;
		rightWall = xPos + radioSqr;
		topWall = yPos - radioSqr;
		bottomWall = yPos + radioSqr;
	end
	
	//Incrementador de las posiciones
	always @(posedge vga_clk) begin
		//Se utiliza el cont para hacer mas 
		//lento/rapido el cuadrado
		if(cont == 10'd600) begin
			if(right) 
				xPos <= xPos + 10'd1;
			if(left) 
				xPos <= xPos - 10'd1;
			if(down) 
				yPos <= yPos + 10'd1;
			if(up)
				yPos <= yPos - 10'd1;
		end
	end

	//Invertir la direccion del cuadrado si llega a cualquiera de las 
	//orillas de la pantalla
	always @(posedge vga_clk) begin
		if(leftWall <= 0) begin
			left <= 1'd0;
			right <= 1'd1;
		end
		if(rightWall >= 640) begin
			left <= 1'd1;
			right <= 1'd0;
		end
		if(topWall <= 0) begin
			up <= 1'd0;
			down <= 1'd1;
		end
		if(bottomWall >= 480) begin
			up <= 1'd1;
			down <= 1'd0;
		end
	end
	
	//Dibujar
	always @(posedge vga_clk) begin	
		//Mientras este dentro del rango establecido por la pantalla
		if (hcount<640 && vcount<480) begin
		//display a colour on the RGB signals
			if((hcount > leftWall) & (hcount < rightWall) & (vcount > topWall) & (vcount < bottomWall)) begin
			//que el hcount se encuentre entre las "paredes" horizontales 
			//y el vcount se encuentre entre las "paredes" verticales
				//Color magenta
				red = 	3'b111;
				green = 	3'b000;
				blue = 	2'b11;
			end
			else begin
				//Color cyan
				red = 	3'b000;
				green = 	3'b111;
				blue = 	2'b11;
			end
		end
		//El resto sera negro
		else begin
			//Color negro
			red = 3'b000;
			green = 3'b000;
			blue = 2'b00;
		end
	end

endmodule

//	r	g	b	color
//	0	0	0	negro	
//	0	0	1	azul
//	0	1	0	verde
//	0	1	1	cyan
//	1	0	0	rojo
//	1	0	1	magenta
//	1	1	0	amarillo
//	1	1	1	blanco
